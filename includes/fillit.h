/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 17:58:53 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/09 17:58:54 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

# define TETRIM_SIZE 26

typedef struct		s_pos
{
	int				x;
	int				y;
}					t_pos;

typedef struct		s_list
{
	char			**tetrim;
	char			c;
	int				num;
	struct s_pos	p;
	struct s_pos	all_p[4];
	struct s_pos	place[4];
	struct s_list	*next;
	int				list_len;
}					t_list;

int					check_column(char *str);
int					valid_symb(char c);
int					valid(char *str);
char				**ft_strsplit(char const *s, char c);
size_t				ft_strlen(char const *str);
void				creat_list(char **map);
void				algoritm(t_list *lst);
int					find_solution(t_list *lst, char **map, int side);
void				print(char **map);
int					can_place(char **map, int side, t_list *lst, t_pos point);
t_pos				get_next(t_list *lst, int x, int y);
void				ft_putchar(char c);
void				ft_putstr(char const *str);
void				freemap(char **map, int side);
void				tet_delete(char **map, t_list *lst);
void				print(char **map);
t_pos				find_free(char **map, int *s, int size);
t_pos				place_v2(char **map, t_list *lst, int side, int *skip);

#endif
