/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algoritm.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 05:41:21 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/14 05:41:21 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_pos	get_next(t_list *lst, int x, int y)
{
	t_pos	next;
	int		i;
	int		j;

	i = 0;
	j = 1;
	while (lst->tetrim[y + i] != NULL)
	{
		while (lst->tetrim[y + i][x + j] != '\0')
		{
			if (lst->tetrim[y + i][x + j] != '.')
			{
				next.y = y + i;
				next.x = x + j;
				return (next);
			}
			j++;
		}
		j = 0 - x;
		i++;
	}
	next.y = -1;
	next.x = -1;
	return (next);
}

int		place(char **map, t_list *lst, int side, int *skip)
{
	t_pos	point;
	t_pos	g_n;
	int		count;

	count = 0;
	g_n = lst->p;
	point = place_v2(map, lst, side, skip);
	if (point.x == -1)
		return (0);
	while (count != 4)
	{
		map[point.y + g_n.y - lst->p.y][point.x + g_n.x - lst->p.x] = lst->c;
		lst->place[count].y = point.y + g_n.y - lst->all_p[0].y;
		lst->place[count].x = point.x + g_n.x - lst->all_p[0].x;
		count++;
		g_n = lst->all_p[count];
	}
	return (1);
}

char	**creat_map(t_list *lst, int *side)
{
	int		n_tet;
	char	**map;
	int		i;
	int		j;

	map = NULL;
	n_tet = lst->list_len;
	if (n_tet * 4 > *side * *side)
		while (n_tet * 4 > *side * *side)
			*side += 1;
	map = (char **)malloc(sizeof(char *) * *side + 1);
	i = 0;
	while (i <= *side)
	{
		map[i] = (char *)malloc(sizeof(char) * *side + 1);
		j = 0;
		while (j < *side)
			map[i][j++] = '.';
		map[i][*side] = '\0';
		j = 0;
		i++;
	}
	map[*side] = NULL;
	return (map);
}

void	algoritm(t_list *lst)
{
	char	**map;
	int		side;

	side = 2;
	map = creat_map(lst, &side);
	while (!(find_solution(lst, map, side)))
	{
		freemap(map, side);
		side++;
		map = creat_map(lst, &side);
	}
	print(map);
}

int		find_solution(t_list *lst, char **map, int side)
{
	int skip;

	skip = 0;
	if (lst->next == NULL)
		return (1);
	while (place(map, lst, side, &skip))
	{
		if (find_solution(lst->next, map, side))
			return (1);
		else
		{
			tet_delete(map, lst);
			skip++;
		}
	}
	return (0);
}
