/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/12 17:42:43 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/12 17:42:43 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static	int		figure(char *str)
{
	int		i;
	int		count;
	int		len;

	if (!str)
		return (0);
	len = ft_strlen(str);
	i = -1;
	count = 0;
	if (check_column(str) == 0)
		return (0);
	while (str[++i] != '\0')
	{
		if (str[i] == '#')
		{
			if (str[i + 1] == '#' && (i + 1) < len)
				count++;
			if (str[i + 5] == '#' && (i + 5) < len)
				count++;
		}
	}
	if (count == 3 || count == 4)
		return (1);
	return (0);
}

static	char	**valid_figure(char *str)
{
	int		i;
	char	**tab;
	int		block;

	i = -1;
	block = 1;
	while (str[++i] != '\0')
		if (str[i] == '\n' && str[i - 1] == '\n')
		{
			str[i] = '0';
			block++;
		}
	tab = ft_strsplit(str, '0');
	i = -1;
	while (++i != block)
	{
		if (!figure(tab[i]))
			return (NULL);
	}
	return (tab);
}

int				valid_symb(char c)
{
	if (c == '#' || c == '\n' || c == '.')
		return (1);
	return (0);
}

static	int		valid_size(char *str)
{
	int		i;
	int		row;

	i = -1;
	row = 0;
	while (str[++i] != '\0')
	{
		row++;
		if (!(valid_symb(str[i])))
			return (0);
		if (str[i] == '\n' && str[i - 1] != '\n' && row != 5)
			return (0);
		if (str[i] == '\n' && str[i - 1] == '\n')
			row--;
		if (row == 5)
			row = 0;
		if (str[i] == '\n' && str[i + 1] == '\0')
		{
			if ((i + 2) % 21 == 0)
				return (1);
			else
				return (0);
		}
	}
	return (0);
}

int				valid(char *str)
{
	char	**map;

	if (!(valid_size(str)))
		return (0);
	map = valid_figure(str);
	if (!map)
		return (0);
	creat_list(map);
	return (1);
}
