/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   support.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/26 18:28:37 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/26 18:28:37 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	freemap(char **map, int side)
{
	int		i;

	i = 0;
	while (i != side - 1)
	{
		free(map[i]);
		i++;
	}
	free(map);
}

void	tet_delete(char **map, t_list *lst)
{
	int count;

	count = 0;
	while (count != 4)
	{
		map[lst->place[count].y][lst->place[count].x] = '.';
		count++;
	}
}

void	print(char **map)
{
	int		i;

	i = 0;
	while (map[i] != NULL)
	{
		ft_putstr(map[i]);
		ft_putstr("\n");
		i++;
	}
}

t_pos	find_free(char **map, int *s, int size)
{
	t_pos	pos;

	pos.y = -1;
	pos.x = -1;
	while (map[*s / size])
	{
		if (map[*s / size][*s % size] == '.')
		{
			pos.y = *s / size;
			pos.x = *s % size;
			return (pos);
		}
		(*s)++;
	}
	return (pos);
}

int		check_column(char *str)
{
	int i;
	int column;
	int	hash;

	i = -1;
	column = 0;
	hash = 0;
	while (str[++i] != '\0')
	{
		if (str[i] == '\n')
			column++;
		if (str[i] == '#')
			hash += 1;
	}
	if (column == 4 && hash == 4)
		return (1);
	return (0);
}
