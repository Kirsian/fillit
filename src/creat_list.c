/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   creat_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 01:32:58 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/14 01:33:00 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static	t_pos	find_position(char **tetrim, char letter, int i, int j)
{
	t_pos	position;

	position.x = -1;
	position.y = -1;
	while (tetrim[i])
	{
		while (tetrim[i][j] != '\0')
		{
			if (tetrim[i][j] == '#')
			{
				if (position.x == -1)
				{
					position.x = j;
					position.y = i;
				}
				tetrim[i][j] = letter;
			}
			j++;
		}
		j = 0;
		i++;
	}
	return (position);
}

static	void	fill_list(t_list *ptr, char *str, int num)
{
	char	**tetrim;
	int		i;
	int		y;
	int		x;

	x = 0;
	y = 0;
	ptr->tetrim = (char **)malloc(sizeof(char *) * 5);
	i = 0;
	tetrim = ft_strsplit(str, '\n');
	while (i < 5)
	{
		ptr->tetrim[i] = (char *)malloc(sizeof(char) * 5);
		ptr->tetrim[i] = tetrim[i];
		i++;
	}
	ptr->num = num;
	ptr->c = 'A' + num;
	ptr->p = find_position(tetrim, 'A' + num, x, y);
	ptr->all_p[0] = ptr->p;
	i = 0;
	while (++i < 4)
		ptr->all_p[i] = get_next(ptr, ptr->all_p[i - 1].x, ptr->all_p[i - 1].y);
}

void			creat_list(char **map)
{
	int		i;
	t_list	*ptr;
	t_list	*lst;
	t_list	*chek;

	ptr = (t_list *)malloc(sizeof(t_list));
	lst = ptr;
	i = 0;
	while (map[i] != NULL)
	{
		fill_list(ptr, map[i], i);
		ptr->next = (t_list *)malloc(sizeof(t_list));
		ptr = ptr->next;
		ptr->next = NULL;
		i++;
	}
	chek = lst;
	while (chek)
	{
		chek->list_len = i;
		chek = chek->next;
	}
	algoritm(lst);
}
