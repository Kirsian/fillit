/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   can_place.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 01:33:11 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/16 01:33:11 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		can_place(char **map, int side, t_list *lst, t_pos point)
{
	int		count;
	t_pos	pos;

	count = 0;
	pos = lst->p;
	while (count != 4)
	{
		if (((pos.y + point.y - lst->p.y) > (side - 1))
		|| ((pos.x + point.x - lst->p.x) > (side - 1)))
			return (0);
		if (map[point.y + pos.y - lst->p.y][point.x + pos.x - lst->p.x] != '.')
			return (0);
		count++;
		pos = lst->all_p[count];
	}
	return (1);
}

t_pos	place_v2(char **map, t_list *lst, int side, int *skip)
{
	t_pos	point;

	point = find_free(map, skip, side);
	if (point.x == -1)
		return (point);
	if (!(can_place(map, side, lst, point)))
	{
		while (point.x > -1)
		{
			if (can_place(map, side, lst, point))
				break ;
			++(*skip);
			point = find_free(map, skip, side);
		}
	}
	return (point);
}
