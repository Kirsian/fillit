/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dzabolot <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 17:53:34 by dzabolot          #+#    #+#             */
/*   Updated: 2016/12/09 17:53:34 by dzabolot         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <unistd.h>

char	*read_file(char *name)
{
	char	*str;
	int		fd;
	int		c;
	int		size;

	if ((fd = open(name, O_RDONLY)) == -1)
		return (NULL);
	c = 1;
	size = 0;
	str = (char *)malloc(sizeof(char) * 1);
	while (c != 0)
	{
		c = read(fd, str, 1);
		size += c;
	}
	close(fd);
	str = (char *)malloc(sizeof(char) * size);
	fd = open(name, O_RDONLY);
	read(fd, str, size);
	str[size] = '\0';
	close(fd);
	return (str);
}

int		main(int argc, char **argv)
{
	char *str;

	if (argc != 2)
	{
		write(1, "usage: ./fillit <argument>", 26);
		return (0);
	}
	if ((str = read_file(argv[1])) == NULL)
	{
		write(1, "error\n", 6);
		return (0);
	}
	if (!valid(str))
	{
		write(1, "error\n", 6);
		return (0);
	}
	return (0);
}
